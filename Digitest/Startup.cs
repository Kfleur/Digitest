﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Digitest.Startup))]
namespace Digitest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
