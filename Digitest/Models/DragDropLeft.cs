﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitest.Models
{
    public class DragDropLeft
    {
[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string LeftText { get; set; }
        public int QuestionId { get; set; }

        public Question Question { get; set; }
    }
}