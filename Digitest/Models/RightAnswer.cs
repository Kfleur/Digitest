﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitest.Models
{
    public class RightAnswer
    {
        public int Id { get; set; }
        [Key, Column(Order = 0)]
        public int QuestionId { get; set; }
        [Key, Column(Order = 1)]
        public int AnswerChoiceId { get; set; }

        public Question Question { get; set; }
        public AnswerChoice AnswerChoice { get; set; }
    }
}