﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Digitest.Models
{

    public enum Gender {[Display(Name = "Mees")] Male, [Display(Name = "Naine")] Female }

    public class Client
    {

        public int Id { get; set; }

        public ICollection<ClientAnswer> ClientAnswer { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli!")]
        [Display(Name = "Eesnimi")]
        public string FirstName { get; set; }

       
        [Display(Name = "Perenimi")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli!")]
        [Display(Name = "Vanus")]
        public int Age { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli!")]
        [Display(Name = "Sugu")]
        public Gender Gender { get; set; }

        [Display(Name = "e-post")]
        public string Email { get; set; }

        [Display(Name = "Nõustun, et minu sisestatud andmed ja testi tulemused salvestatakse ning et neid tohib töödelda ja üldistatud kujul avaldada.")]
        public bool Permission { get; set; }

    }
}