﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitest.Models
{
    public class Question
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public ICollection<RightAnswer> RightAnswer { get; set; }
        public ICollection<ClientAnswer> ClientAnswer { get; set; }
        public ICollection<AnswerChoice> AnswerChoice { get; set; }

        public AnswerType AnswerType { get; set; }
        public int? AnswerTypeId { get; set; }

        [DataType(DataType.MultilineText)]
        public string QuestionText { get; set; }
        
        public ICollection<DragDropLeft> DragDropLeft { get; set; }
        public ICollection<DragDropRight> DragDropRight { get; set; }

    }
}