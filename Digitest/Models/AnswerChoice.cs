﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitest.Models
{
    public class AnswerChoice
    {
[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public Question Question { get; set; }

        [DataType(DataType.MultilineText)]
        public string AnswerText { get; set; }
        
        public int? QuestionId { get; set; }
        [NotMapped]
        public bool IsRightAnswer { get; set; }

    }
}