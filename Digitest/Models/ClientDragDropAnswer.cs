﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitest.Models
{
    public class ClientDragDropAnswer
    {
        [Key, Column(Order = 0)]
        public int DragDropLeftId { get; set; }
        [Key, Column(Order = 1)]
        public int DragDropRightId { get; set; }
        [Key, Column(Order = 2)]
        public int ClientId { get; set; }

        public Client Client { get; set; }
        public DragDropLeft DragDropLeft { get; set; }
        public DragDropRight DragDropRight { get; set; }
    }
}