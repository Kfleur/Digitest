﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitest.Models
{
    public class ClientQuestions
    {
        public Client Client { get; set; }
        public ICollection<Question> Questions { get; set; }

    }
}