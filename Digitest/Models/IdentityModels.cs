﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Digitest.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }


    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Digitest.Models.AnswerChoice> AnswerChoice { get; set; }
        public DbSet<Digitest.Models.AnswerType> AnswerType { get; set; }
        public DbSet<Digitest.Models.Client> Client { get; set; }
        public DbSet<Digitest.Models.ClientAnswer> ClientAnswer { get; set; }
        public DbSet<Digitest.Models.Question> Question { get; set; }
        public DbSet<Digitest.Models.RightAnswer> RightAnswer { get; set; }
        public DbSet<Digitest.Models.DragDropLeft> DragDropLeft { get;  set; }
        public DbSet<Digitest.Models.DragDropRight> DragDropRight { get; set; }
        public DbSet<Digitest.Models.ClientDragDropAnswer> ClientDragDropAnswer { get; set; }
        public DbSet<Digitest.Models.DragDrop> DragDrop { get; set; }
    }
}