namespace Digitest.Migrations
{
    using Digitest.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Digitest.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Digitest.Models.ApplicationDbContext context)
        {
            
            if (!context.AnswerType.Any())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var answertypes = new List<AnswerType>
                    {
                        new AnswerType { TypeName = "CheckBox", Id = 1 },
                        new AnswerType { TypeName = "RadioButton", Id = 2 },
                        new AnswerType { TypeName = "TextArea", Id = 3 },
                        new AnswerType { TypeName = "DragDrop", Id = 4 }
                    };
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[AnswerTypes] On ");
                    context.AnswerType.AddRange(answertypes);
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[AnswerTypes] Off ");

                    transaction.Commit();
                }
            }
            
            if (!context.Question.Any())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var questions = new List<Question>
                    {
                        new Question { QuestionText = "Milline nendest on k�ige turvalisem parool?", AnswerTypeId = 2, Id = 1 },
                        new Question { QuestionText = "Otsi internetist j�rgnev info: mis aastal moodustati esimest korda " +
                            "Eesti alaline esindus Rahvasteliidus?", AnswerTypeId = 3, Id = 2 },
                        new Question { QuestionText = "�henda paarid: vii tarkvara kokku selle peamise funktsionaalsusega",
                            AnswerTypeId = 4, Id = 3 },
                        new Question { QuestionText = "Millise liidese kaudu on enamasti v�imalik arvutile k�lge �hendada " +
                            "lisaseadmeid, nt hiir, klaviatuur, id-kaardi lugeja, veebikaamera jne?", AnswerTypeId = 2, Id = 4 },
                        new Question { QuestionText = "Vaatad TV-st saadet ja saatejuht kutsub �les Twitteris postitama, " +
                            "kasutades m�rks�na #Saatenimi. Mida see t�hendab?", AnswerTypeId = 2 ,Id = 5 },
                        new Question { QuestionText = "Kas arvutid ja nutiseadmed vajavad tarkvarauuendusi?", AnswerTypeId = 2,
                            Id = 6 },
                        new Question { QuestionText = "N�ed poes meelep�rast m��blieset, aga soovid enne ostmist eemal viibiva " +
                            "pereliikme arvamust k�sida. Kuidas seda k�ige lihtsamini ja kiiremini teha?", AnswerTypeId = 2, Id = 7 },
                        new Question { QuestionText = "Oled liikvel ja Sul on kaasas kaasaegne nutitelefon ja s�learvuti. " +
                            "Nutitelefon on mobiilse internetiga �hendatud. Soovid ka arvutiga internetti kasutada, aga l�heduses pole " +
                            "�htegi WiFi v�rku. Mida teed?", AnswerTypeId = 2, Id = 8 },
                        new Question { QuestionText = "Millistel komponentidel v�ivad paikneda olulised/ tundlikud andmed?",
                            AnswerTypeId = 1, Id = 9 },
                        new Question { QuestionText = "Kas k�iki e-kirjaga saadetud manuseid v�ib julgelt avada?",
                            AnswerTypeId = 2, Id = 10 },
                        new Question { QuestionText = "Mida saab ID-kaardiga teha?", AnswerTypeId = 1, Id = 11 },
                        new Question { QuestionText = "�henda paarid: vii veebisait kokku selle peamise funktsionaalsusega",
                            AnswerTypeId = 4, Id = 12 },
                        new Question { QuestionText = "Soovid nutiseadmesse uut rakendust (ehk �ppi) paigaldada. Kas ja mida " +
                            "silmas pidada?", AnswerTypeId = 2, Id = 13 },
                        new Question { QuestionText = "Kas internetis leidub v��ra v�i eksitavat infot, sh nt libauudiseid?",
                            AnswerTypeId = 2, Id = 14 },
                        new Question { QuestionText = "Mida tasub arvestada sotsiaalmeediasse (nt Facebooki v�i Instagrami) " +
                            "postitamisel?", AnswerTypeId = 1, Id = 15 },
                        new Question { QuestionText = "Soovid s�brale arvutis leiduvaid pilte jagada, aga failimaht on e-kirjaga " +
                            "saatmiseks liiga suur. Mida saad teha, et k�ik pildid ikkagi s�brani j�uaks?", AnswerTypeId = 1, Id = 16 },
                        new Question { QuestionText = "�henda paarid: vii faililaiend kokku �ige failit��biga", AnswerTypeId = 4,
                            Id = 17 },
                        new Question { QuestionText = "Soovid internetis varjatud liigutusi teha, valid veebilehitseja vastava " +
                            "re�iimi �varjatud lehitsemine� (private, browsing, incognito vm). Millised v�ited on t�esed?", AnswerTypeId = 1, Id = 18 },
                        new Question { QuestionText = "Soovid veebisaiti luua. Milline v�ide on t�ene?", AnswerTypeId = 2, Id = 19 },
                        new Question { QuestionText = "Kas ja miks on arvuti (nt WIN+L) v�i nutiseadme lukustamine (nt ekraanilukk)" +
                            " vajalik?", AnswerTypeId = 2, Id = 20 },
                        new Question { QuestionText = "Kas k�iki internetis leiduvaid faile v�ib vabalt kasutada ja edasi jagada?",
                            AnswerTypeId = 2, Id = 21 },
                        new Question { QuestionText = "Kui soovid Tallinnas Viru Keskuse juurest jalgsi teletorni juurde minna, " +
                            "siis kui kaua aega selleks keskmiselt kulub?", AnswerTypeId = 2, Id = 22 },
                    };
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Questions] On ");
                    context.Question.AddRange(questions);
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Questions] Off ");

                    transaction.Commit();
                }
            }

            if (!context.AnswerChoice.Any())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var answerchoices = new List<AnswerChoice>
                    {
                        new AnswerChoice { AnswerText = "qwerty", QuestionId = 1, Id = 1 },
                        new AnswerChoice { AnswerText = "L!mP@L!m0n@@D", QuestionId = 1, Id = 2 },
                        new AnswerChoice { AnswerText = "K2sk@NV2n3M_Qi_M3!3", QuestionId = 1, Id = 3 },
                        new AnswerChoice { AnswerText = "0000", QuestionId = 1, Id = 4 },
                        new AnswerChoice { AnswerText = "S�nniaasta, nt 1982", QuestionId = 1, Id = 5 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 1, Id = 6 },
                        new AnswerChoice { AnswerText = "1931", QuestionId = 2, Id = 7 },
                        new AnswerChoice { AnswerText = "HDMI", QuestionId = 4, Id = 8 },
                        new AnswerChoice { AnswerText = "VGA", QuestionId = 4, Id = 9 },
                        new AnswerChoice { AnswerText = "HTTPS", QuestionId = 4, Id = 10 },
                        new AnswerChoice { AnswerText = "USB", QuestionId = 4, Id = 11 },
                        new AnswerChoice { AnswerText = "AUX", QuestionId = 4, Id = 12 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 4, Id = 13 },
                        new AnswerChoice { AnswerText = "# (hashtag ehk teemaviide) v�imaldab oma e-posti kontole sisse logida, et seel�bi saatejuhi k�simusele vastata.", QuestionId = 5, Id = 14 },
                        new AnswerChoice { AnswerText = "# (hashtag ehk teemaviide) on levinud viis, kuidas sotsiaalmeedias (nt Twitteris) postitusi teemaga siduda; tihti kuvatakse korrektse teemaviitega s�numeid ka nt televisiooni otsesaadetes, et inimesed saaksid arvamust avaldada.", QuestionId = 5, Id = 15 },
                        new AnswerChoice { AnswerText = "Tegemist on tehnilise ligip��sukoodiga, et saadet saaks vaadata ka nutiseadmetelt. Korrektne # (hastag ehk teemaviide) tuleb sisestada veebilehitseja aadressiribale.", QuestionId = 5, Id = 16 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 5, Id = 17 },
                        new AnswerChoice { AnswerText = "Ei, sest neil on toimiv tarkvara juba olemas", QuestionId = 6, Id = 18 },
                        new AnswerChoice { AnswerText = "Jah, sest tarkvarauuendused aitavad tagada turvalisust ja t��kindlust ning v�ivad sisaldada t�iendavat funktsionaalsust ", QuestionId = 6, Id = 19 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 6, Id = 20 },
                        new AnswerChoice { AnswerText = "Teen nutitelefoniga/tahvelarvutiga esemest pildi ja saadan selle e-kirjaga v�i sotsiaalmeedia (nt Messenger) vahendusel pereliikmele, et arvamust k�sida.", QuestionId = 7, Id = 21 },
                        new AnswerChoice { AnswerText = "Palun m��jal eseme broneerida ning tulen j�rgmisel korral koos pereliikmega tagasi.", QuestionId = 7, Id = 22 },
                        new AnswerChoice { AnswerText = "Teen nutitelefoniga/tahvelarvutiga esemest pildi ja n�itan seda kodus pereliikmele. Tulen teisel p�eval uuesti poodi ostu vormistama.", QuestionId = 7, Id = 23 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 7, Id = 24 },
                        new AnswerChoice { AnswerText = "Kasutan internetti nutitelefonis, sest arvuti internetti �hendamise v�imalus puudub.", QuestionId = 8, Id = 25 },
                        new AnswerChoice { AnswerText = "Loon nutitelefoniga kaugp��supunkti (hotspot), mille kaudu saan ka arvuti internetti �hendada.", QuestionId = 8, Id = 26 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 8, Id = 27 },
                        new AnswerChoice { AnswerText = "Hiir", QuestionId = 9, Id = 28 },
                        new AnswerChoice { AnswerText = "Ekraan", QuestionId = 9, Id = 29 },
                        new AnswerChoice { AnswerText = "K�vaketas", QuestionId = 9, Id = 30 },
                        new AnswerChoice { AnswerText = "Klaviatuur", QuestionId = 9, Id = 31 },
                        new AnswerChoice { AnswerText = "M�lupulk", QuestionId = 9, Id = 32 },
                        new AnswerChoice { AnswerText = "ID-kaardi lugeja", QuestionId = 9, Id = 33 },
                        new AnswerChoice { AnswerText = "M�lukaart", QuestionId = 9, Id = 34 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 9, Id = 35 },
                        new AnswerChoice { AnswerText = "Jah, sest need on t�nap�eval p�hjalikult eelkontrollitud", QuestionId = 10, Id = 36 },
                        new AnswerChoice { AnswerText = "Ei, sest need v�ivad sisaldada viiruseid ja pahavara", QuestionId = 10, Id = 37 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 10, Id = 38 },
                        new AnswerChoice { AnswerText = "Erinevatesse keskkondadesse (nt internetipanka) turvaliselt siseneda", QuestionId = 11, Id = 39 },
                        new AnswerChoice { AnswerText = "Pangaautomaadist sularaha v�lja v�tta", QuestionId = 11, Id = 40 },
                        new AnswerChoice { AnswerText = "Dokumente allkirjastada", QuestionId = 11, Id = 41 },
                        new AnswerChoice { AnswerText = "Telefoniga ostu sooritada", QuestionId = 11, Id = 42 },
                        new AnswerChoice { AnswerText = "Teiste inimeste ID-kaartide paroole n�ha", QuestionId = 11, Id = 43 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 11, Id = 44 },
                        new AnswerChoice { AnswerText = "Alati tasub vaadata �le teiste kasutajate hinnangud, kommentaarid, rakenduse poolt soovitud ligip��sud seadmes, allalaadimiste arv jne.", QuestionId = 13, Id = 45 },
                        new AnswerChoice { AnswerText = "K�ik rakendused, mis rakenduste poodidest leitavad on, on turvalised ja ohutud. Midagi t�iendavalt silmas pidama ei pea.", QuestionId = 13, Id = 46 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 13, Id = 47 },
                        new AnswerChoice { AnswerText = "Ei, sest internetis leviv sisu on rangelt kontrollitud.", QuestionId = 14, Id = 48 },
                        new AnswerChoice { AnswerText = "Jah, sest internetis on v�ga lihtne eksitavat infot avaldada.", QuestionId = 14, Id = 49 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 14, Id = 50 },
                        new AnswerChoice { AnswerText = "Postituste sisu v�ib j�uda soovimatute isikuteni (nt vargad n�evad, et oled kodust �ra l�inud).", QuestionId = 15, Id = 51 },
                        new AnswerChoice { AnswerText = "Vaid paari vale liigutusega v�ite kogemata postitada sisu v�i pilte, mida tegelikult ei soovinud.", QuestionId = 15, Id = 52 },
                        new AnswerChoice { AnswerText = "Sotsiaalmeedia puhul ohud puuduvad, sest need on vaid s�bralikud piltide ja emotsioonide jagamise keskkonnad.", QuestionId = 15, Id = 53 },
                        new AnswerChoice { AnswerText = "Kuigi saate hiljem oma pilte ja postitusi kustutada, siis keegi kolmas v�ib olla need endale juba salvestanud, seega t�ielikku kustutamise v�imalust ei olegi.", QuestionId = 15, Id = 54 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 15, Id = 55 },
                        new AnswerChoice { AnswerText = "Salvestad pildid m�lupulgale ning annad koos pulgaga s�brale.", QuestionId = 16, Id = 56 },
                        new AnswerChoice { AnswerText = "Saadad s�brale vaid �he pildi ning loobud teiste jagamisest.", QuestionId = 16, Id = 57 },
                        new AnswerChoice { AnswerText = "Kasutad m�nd failijagamise keskkonda, nt Google Drive, Dropbox vm.", QuestionId = 16, Id = 58 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 16, Id = 59 },
                        new AnswerChoice { AnswerText = "Internetis tehtavad liigutused on n��d 100% salajased.", QuestionId = 18, Id = 60 },
                        new AnswerChoice { AnswerText = "Lehitsemise ajalugu ei salvestata hiljuti k�lastatud lehek�lgedesse.", QuestionId = 18, Id = 61 },
                        new AnswerChoice { AnswerText = "Teenusepakkuja (ISP) n�eb ikka seda, mida internetis teed.", QuestionId = 18, Id = 62 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 18, Id = 63 },
                        new AnswerChoice { AnswerText = "K�igil on t�nap�eval v�imalus veebisaite luua, aga luua saab ainult �he veebisaidi inimese kohta.", QuestionId = 19, Id = 64 },
                        new AnswerChoice { AnswerText = "Veebisaite saavad luua ainult juriidilised isikud, nt O� v�i AS.", QuestionId = 19, Id = 65 },
                        new AnswerChoice { AnswerText = "K�igil on t�nap�eval v�imalus veebisaite luua ning selleks on rohkelt eri variante.", QuestionId = 19, Id = 66 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 19, Id = 67 },
                        new AnswerChoice { AnswerText = "Jah, on vajalik, et kaitsta seadet ja oma andmeid v��raste eest.", QuestionId = 20, Id = 68 },
                        new AnswerChoice { AnswerText = "Ei, see pole vajalik, sest k�ik kaasaegsed seadmed omavad s�rmej�ljelugejat v�i kaamerap�hist tuvastust, mis identifitseerivad automaatselt �ige kasutaja.", QuestionId = 20, Id = 69 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 20, Id = 70 },
                        new AnswerChoice { AnswerText = "Jah, muidugi � internet ongi failide jagamiseks loodud.", QuestionId = 21, Id = 71 },
                        new AnswerChoice { AnswerText = "Ei, internetis levivatele failidele kehtivad autori�igused, millega tuleb arvestada.", QuestionId = 21, Id = 72 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 21, Id = 73 },
                        new AnswerChoice { AnswerText = "Umbes 30 minutit", QuestionId = 22, Id = 74 },
                        new AnswerChoice { AnswerText = "Umbes 1 tund", QuestionId = 22, Id = 75 },
                        new AnswerChoice { AnswerText = "Umbes 1,5 tundi", QuestionId = 22, Id = 76 },
                        new AnswerChoice { AnswerText = "Umbes 2 tundi", QuestionId = 22, Id = 77 },
                        new AnswerChoice { AnswerText = "Umbes 2,5 tundi", QuestionId = 22, Id = 78 },
                        new AnswerChoice { AnswerText = "Umbes 3 tundi", QuestionId = 22, Id = 79 },
                        new AnswerChoice { AnswerText = "Ei tea", QuestionId = 22, Id = 80 }
                    };
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[AnswerChoices] On ");
                    context.AnswerChoice.AddRange(answerchoices);
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[AnswerChoices] Off ");

                    transaction.Commit();
                }
            }

            if (!context.RightAnswer.Any())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var rightAnswers = new List<RightAnswer>
                    {
                        new RightAnswer { QuestionId = 1, AnswerChoiceId = 3 },
                        new RightAnswer { QuestionId = 2, AnswerChoiceId = 6 },
                       
                        new RightAnswer { QuestionId = 4, AnswerChoiceId = 11 },
                        new RightAnswer { QuestionId = 5, AnswerChoiceId = 15 },
                        new RightAnswer { QuestionId = 6, AnswerChoiceId = 19 },
                        new RightAnswer { QuestionId = 7, AnswerChoiceId = 21 },
                        new RightAnswer { QuestionId = 8, AnswerChoiceId = 26 },
                        new RightAnswer { QuestionId = 9, AnswerChoiceId = 30 },
                        new RightAnswer { QuestionId = 9, AnswerChoiceId = 32 },
                        new RightAnswer { QuestionId = 9, AnswerChoiceId = 34 },
                        new RightAnswer { QuestionId = 10, AnswerChoiceId = 37 },
                        new RightAnswer { QuestionId = 11, AnswerChoiceId = 39 },
                        new RightAnswer { QuestionId = 11, AnswerChoiceId = 41 },
                        
                        new RightAnswer { QuestionId = 13, AnswerChoiceId = 45 },
                        new RightAnswer { QuestionId = 14, AnswerChoiceId = 49 },
                        new RightAnswer { QuestionId = 15, AnswerChoiceId = 51 },
                        new RightAnswer { QuestionId = 15, AnswerChoiceId = 52 },
                        new RightAnswer { QuestionId = 15, AnswerChoiceId = 54 },
                        new RightAnswer { QuestionId = 16, AnswerChoiceId = 56 },
                        new RightAnswer { QuestionId = 16, AnswerChoiceId = 58 },
                        
                        new RightAnswer { QuestionId = 18, AnswerChoiceId = 61 },
                        new RightAnswer { QuestionId = 18, AnswerChoiceId = 62 },
                        new RightAnswer { QuestionId = 19, AnswerChoiceId = 66 },
                        new RightAnswer { QuestionId = 20, AnswerChoiceId = 68 },
                        new RightAnswer { QuestionId = 21, AnswerChoiceId = 72 },
                        new RightAnswer { QuestionId = 22, AnswerChoiceId = 77 }
                    };

                    context.RightAnswer.AddRange(rightAnswers);
                    context.SaveChanges();

                    transaction.Commit();
                }
            }

            if (!context.DragDropLeft.Any())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var dragdroplefts = new List<DragDropLeft>
                    {
                        new DragDropLeft { LeftText = "MS Office", QuestionId = 3, Id = 1 },
                        new DragDropLeft { LeftText = "MS Excel / Google Sheets", QuestionId = 3, Id = 2 },
                        new DragDropLeft { LeftText = "MS Word / Google Docs", QuestionId = 3, Id = 3 },
                        new DragDropLeft { LeftText = "MS PowerPoint / Google Slides ", QuestionId = 3, Id = 4 },
                        new DragDropLeft { LeftText = "MS Outlook", QuestionId = 3, Id = 5 },
                        new DragDropLeft { LeftText = "Windows 10", QuestionId = 3, Id = 6 },
                        new DragDropLeft { LeftText = "Skype", QuestionId = 3, Id = 7 },
                        new DragDropLeft { LeftText = "Chrome / Edge / Internet Explorer / Firefox jpt", QuestionId = 3, Id = 8 },
                        new DragDropLeft { LeftText = "Google.ee", QuestionId = 12, Id = 9 },
                        new DragDropLeft { LeftText = "Suhtlus.ee", QuestionId = 12, Id = 10 },
                        new DragDropLeft { LeftText = "Neti.ee", QuestionId = 12, Id = 11 },
                        new DragDropLeft { LeftText = "Dropbox.com", QuestionId = 12, Id = 12 },
                        new DragDropLeft { LeftText = "Gmail.com ", QuestionId = 12, Id = 13 },
                        new DragDropLeft { LeftText = "Koolitus.ee", QuestionId = 12, Id = 14 },
                        new DragDropLeft { LeftText = "Drive.google.com", QuestionId = 12, Id = 15 },
                        new DragDropLeft { LeftText = "Err.ee", QuestionId = 12, Id = 16 },
                        new DragDropLeft { LeftText = ".jpg/ .jpeg / .png", QuestionId = 17, Id = 17 },
                        new DragDropLeft { LeftText = ".doc/.docx", QuestionId = 17, Id = 18 },
                        new DragDropLeft { LeftText = ".pdf", QuestionId = 17, Id = 19 },
                        new DragDropLeft { LeftText = ".gif", QuestionId = 17, Id = 20 },
                        new DragDropLeft { LeftText = ".exe", QuestionId = 17, Id = 21 },
                        new DragDropLeft { LeftText = ".mp4 / .avi", QuestionId = 17, Id = 22 },
                        new DragDropLeft { LeftText = ".xlxs / .xls", QuestionId = 17, Id = 23 }
                    };
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[DragDropLefts] On ");
                    context.DragDropLeft.AddRange(dragdroplefts);
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[DragDropLefts] Off ");

                    transaction.Commit();
                }
            }

            if (!context.DragDropRight.Any())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var dragdroprights = new List<DragDropRight>
                    {
                        new DragDropRight { RightText = "levinud kontoritarkvara, mis sisaldab endas programme, nt MS Excel, MS Word, MS PowerPoint jt", QuestionId = 3, Id = 1 },
                        new DragDropRight { RightText = "tabelarvutus", QuestionId = 3, Id = 2 },
                        new DragDropRight { RightText = "tekstit��tlus", QuestionId = 3, Id = 3 },
                        new DragDropRight { RightText = "esitlused", QuestionId = 3, Id = 4 },
                        new DragDropRight { RightText = "e-kirjad ja kalender", QuestionId = 3, Id = 5 },
                        new DragDropRight { RightText = "operatsioonis�steem", QuestionId = 3, Id = 6 },
                        new DragDropRight { RightText = "s�numid ja suhtlemine", QuestionId = 3, Id = 7 },
                        new DragDropRight { RightText = "veebilehitsemine", QuestionId = 3, Id = 8 },
                        new DragDropRight { RightText = "otsing", QuestionId = 12, Id = 9 },
                        new DragDropRight { RightText = "e-kirjad", QuestionId = 12, Id = 10 },
                        new DragDropRight { RightText = "otsing", QuestionId = 12, Id = 11 },
                        new DragDropRight { RightText = "failide majutus ja jagamine pilves", QuestionId = 12, Id = 12 },
                        new DragDropRight { RightText = "e-kirjad", QuestionId = 12, Id = 13 },
                        new DragDropRight { RightText = "IT alased koolitused", QuestionId = 12, Id = 14 },
                        new DragDropRight { RightText = "failide majutus ja jagamine pilves", QuestionId = 12, Id = 15 },
                        new DragDropRight { RightText = "uudised ja meelelahutus", QuestionId = 12, Id = 16 },
                        new DragDropRight { RightText = "pildifail", QuestionId = 17, Id = 17 },
                        new DragDropRight { RightText = "tekstidokument", QuestionId = 17, Id = 18 },
                        new DragDropRight { RightText = "PDF dokument", QuestionId = 17, Id = 19 },
                        new DragDropRight { RightText = "animatsioon", QuestionId = 17, Id = 20 },
                        new DragDropRight { RightText = "programmi k�ivitamine", QuestionId = 17, Id = 21 },
                        new DragDropRight { RightText = "videofail", QuestionId = 17, Id = 22 },
                        new DragDropRight { RightText = "Exceli fail (tabel)", QuestionId = 17, Id = 23 }
                    };
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[DragDropRights] On ");
                    context.DragDropRight.AddRange(dragdroprights);
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[DragDropRights] Off ");

                    transaction.Commit();
                }
            }

            if (!context.DragDrop.Any())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    var dragdrops = new List<DragDrop>
                    {
                        new DragDrop { DragDropLeftId = 1, DragDropRightId = 1 },
                        new DragDrop { DragDropLeftId = 2, DragDropRightId = 2 },
                        new DragDrop { DragDropLeftId = 3, DragDropRightId = 3 },
                        new DragDrop { DragDropLeftId = 4, DragDropRightId = 4 },
                        new DragDrop { DragDropLeftId = 5, DragDropRightId = 5 },
                        new DragDrop { DragDropLeftId = 6, DragDropRightId = 6 },
                        new DragDrop { DragDropLeftId = 7, DragDropRightId = 7 },
                        new DragDrop { DragDropLeftId = 8, DragDropRightId = 8 },
                        new DragDrop { DragDropLeftId = 9, DragDropRightId = 9 },
                        new DragDrop { DragDropLeftId = 10, DragDropRightId = 10 },
                        new DragDrop { DragDropLeftId = 11, DragDropRightId = 11 },
                        new DragDrop { DragDropLeftId = 12, DragDropRightId = 12 },
                        new DragDrop { DragDropLeftId = 13, DragDropRightId = 13 },
                        new DragDrop { DragDropLeftId = 14, DragDropRightId = 14 },
                        new DragDrop { DragDropLeftId = 15, DragDropRightId = 15 },
                        new DragDrop { DragDropLeftId = 16, DragDropRightId = 16 },
                        new DragDrop { DragDropLeftId = 17, DragDropRightId = 17 },
                        new DragDrop { DragDropLeftId = 18, DragDropRightId = 18 },
                        new DragDrop { DragDropLeftId = 19, DragDropRightId = 19 },
                        new DragDrop { DragDropLeftId = 20, DragDropRightId = 20 },
                        new DragDrop { DragDropLeftId = 21, DragDropRightId = 21 },
                        new DragDrop { DragDropLeftId = 22, DragDropRightId = 22 },
                        new DragDrop { DragDropLeftId = 23, DragDropRightId = 23 }

                    };

                    context.DragDrop.AddRange(dragdrops);
                    context.SaveChanges();

                    transaction.Commit();
                }
            }
            
       
            //var passwordHash = new PasswordHasher();
            //string password = passwordHash.HashPassword("Valiit123!");
            //context.Users.AddOrUpdate(u => u.UserName,
            //    new ApplicationUser
            //    {
            //        UserName = "digi@mail.ee",
            //        PasswordHash = password,

            //    });

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            string[] Roles = { "Admin", "User" };
            IdentityResult roleResult;

            foreach (var roles in Roles)
            {
                if (!roleManager.RoleExists(roles))
                {
                    roleResult = roleManager.Create(new IdentityRole(roles));
                }
            }

            //var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            //userManager.AddToRole("b05526a6-8ba4-48eb-94c5-8af04bf38853", "Admin");

        }
    }
}

