﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Digitest.Models;

namespace Digitest.Controllers
{
    public class AnswerChoicesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: AnswerChoices
        public ActionResult Index()
        {
           
                var answerChoice = db.AnswerChoice.Include(a => a.Question).Include(s => s.Question.AnswerType);
                return View(answerChoice.ToList());
           
           
        }

        // GET: AnswerChoices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnswerChoice answerChoice = db.AnswerChoice.Find(id);
            if (answerChoice == null)
            {
                return HttpNotFound();
            }
            return View(answerChoice);
        }

        // GET: AnswerChoices/Create
        public ActionResult Choices(int? questionId)
        {
            

            List<AnswerChoice> answerChoices = new List<AnswerChoice> { new AnswerChoice { Id = 0, AnswerText = "", IsRightAnswer = false, QuestionId = questionId } };
            

            return View(answerChoices);
        }
        public ActionResult DragDropChoices(int questionId)
        {


            List<DragDrop> dragDrops = new List<DragDrop> { new DragDrop
            {
                DragDropLeft = new DragDropLeft()
                {
                    LeftText = "Vasakpoolne",
                    QuestionId = questionId

                },
                 DragDropRight = new DragDropRight()
                {
                    RightText = "Parempoolne",
                    QuestionId = questionId
                }
               } };


            return View(dragDrops);
        }
        // POST: AnswerChoices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Choices(List<AnswerChoice> answerChoices)
        {

            if (ModelState.IsValid)
            {

                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    foreach (var i in answerChoices)
                    {
                        //Question question = db.Question.Find(questionId);
                        db.AnswerChoice.Add(i);
                        db.SaveChanges();

                        if (i.IsRightAnswer)
                        {
                            var rightAnswer = new RightAnswer();
                            rightAnswer.QuestionId = (int)i.QuestionId;
                            rightAnswer.AnswerChoiceId = i.Id;
                            db.RightAnswer.Add(rightAnswer);
                     
                        }
                    }
                    db.SaveChanges();

                    //if (questionId != null)
                    //{

                    //    var answerChoice = new AnswerChoice() {QuestionId = questionId };
                    //    db.AnswerChoice.Add(answerChoice);
                    //    db.SaveChanges();
                    //}

                    ViewBag.Message = "Vastused salvestatud!";
                    ModelState.Clear();
                    answerChoices = new List<AnswerChoice> { new AnswerChoice { Id = 0, AnswerText = "", IsRightAnswer = false } };
                }

            }
            return View(answerChoices);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DragDropChoices(List<DragDrop> dragDrops)
        {

            if (ModelState.IsValid)
            {

                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    foreach (var i in dragDrops)
                    {
                        //Question question = db.Question.Find(questionId);
                        db.DragDropLeft.Add(i.DragDropLeft);
                        db.DragDropRight.Add(i.DragDropRight);

                        db.SaveChanges();
                        var dragdrop = new DragDrop();
                        dragdrop.DragDropLeftId = i.DragDropLeft.Id;
                        dragdrop.DragDropRightId = i.DragDropRight.Id;

                        
                    }
                    db.SaveChanges();
                    

                    ViewBag.Message = "Vastused salvestatud!";
                    ModelState.Clear();
                    return RedirectToAction("Index", "AnswerChoices");
                }

            }
            return View(dragDrops);
        }
        //GET: AnswerChoices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnswerChoice answerChoice = db.AnswerChoice.Find(id);
            if (answerChoice == null)
            {
                return HttpNotFound();
            }
            ViewBag.QuestionId = new SelectList(db.Question, "Id", "QuestionText", answerChoice.QuestionId);
            //ViewBag.AnswerTypeId = new SelectList(db.AnswerType, "Id", "TypeName", answerChoice.Question.AnswerTypeId);

            return View(answerChoice);
        }

        // POST: AnswerChoices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,AnswerText,QuestionId")] AnswerChoice answerChoice, AnswerType answerType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(answerChoice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.QuestionId = new SelectList(db.Question, "Id", "QuestionText", answerChoice.QuestionId);
            ViewBag.AnswerTypeId = new SelectList(db.AnswerType, "Id", "TypeName", answerChoice.Question.AnswerTypeId);
            return View(answerChoice);
        }

        // GET: AnswerChoices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnswerChoice answerChoice = db.AnswerChoice.Find(id);
            if (answerChoice == null)
            {
                return HttpNotFound();
            }
            return View(answerChoice);
        }

        // POST: AnswerChoices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AnswerChoice answerChoice = db.AnswerChoice.Find(id);
            db.AnswerChoice.Remove(answerChoice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
